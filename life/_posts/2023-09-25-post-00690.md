---
layout: post
title: "여름 성경학교 준비 가렌드 만들기 풍선장식"
toc: true
---

 우리 교회엔 여름마다 행사들이 줄줄이~
 여름성경 학교, 단기 선교, 징검다리 아부 수련회~
 ​
 내가 봉사하는 초등2부의

 하시 성경학교를 위하여 작은 근력 기부~

 알고 보면 심히 쉬운 가렌드 만들기
 원하는 문구 사진필름 하고~
  클리터 펠트지에 글 대고 오리기~~​​
 ​
 나의 목표는 초등2부 예배실을
 화사한 분위기로 만들어주는것~
 노란 펠트지는 미상불 효과짱
 글리터펠트지 뒷부분은 스티커처럼
  되어있어서 붙이기가 매우 쉽다
 규실 하던 가위질 하니
 손목이 쪼매 아푸긴 하지만😅​​
 ​
 근간히 풍선도 불고~~
 커피도 마시고 맛있는 빵도 먹음 ㅎㅎ
 준비하는 무심히 찬양소리
 웃음소리가 끊이질 않았다
 본마음 달성~ 밝고 환하니 좋다
 손수 보면 더욱더욱 이쁜데~~​​
 ​
 아이들이 자신 색칠하여 완성한

 멋진 절경 걸어주니 월등히 더욱더 밝아졌네
 아이들이 바람 맞대고 함께 만든
 작품이라 의미가 있다
 ​
 한장한장 나눠서 색칠하고 합체하기
 함께 색칠하여 큰그림도 쉽게  완성~
 색은 알록달록 십중팔구 달라도 조화를
  이루어내는 놀라운 정말로 발견😁​​
 ​
 삼삼오오 모여서 색칠하는 아이들~
 고사리 같은 손으로 야무지게 하는
 이삐들 사랑스러웠다~^^​​
 ​
 맛 살려주는 만국기도 달아주구
 국기들 보며 열방을 품고 기도해야지~
 ​
 고담 전하시는 목사님~
 말씀 듣는 초등3.4학년 아이들🧡​​
 ​
 입구에는 아이들을 환영하는
 맘을 풍선으로 표현하기~^^​​
 ​
 선생님들이 일층 신났어 ㅎㅎ
 소녀처럼 해맑으심 😁
 ​
 얘들아 얼른얼른 와라
 두팔벌려 너희들을 환영해~~🎉​​
 ​
 점심으로 맛난 해물짬뽕 먹기 ~^^
 봉사하고 먹으니 더더욱 꿀맛 ㅎㅎ
 국물이 끝내줘요
 짬뽕에 해물 듬뿍~참 은혜롭다😁​​
 ​
 아이들의 재치와 생각들을
 알아보기 위해 마련한 코너~
 내일쯤은 그뜩그뜩 [풍선장식](https://scarfdraconian.com/life/post-00058.html) 가득가득 채워지겠지?!
 ㅋㅋㅋ 짜장 아이들의 재치는 못 말려
 뽑아서 선물 준다고 했는데
 당첨의 행운은 누구에게 돌아갈런지~^^​​
 ​
 장래 꿈나무들~ 주님의 귀한 자녀들~
 퍽 사랑스러운 아이들의
 얼굴은 가리고 싶지 않네 😅
 ​
 일모 부흥회 시간때 반주하며
 입을 모아 찬양하는 아이들을 보고
 큰소리로 꽉 찬양하는 고함 들으니
 가슴이 인제 뜨거워져 울컥했어 ㅜㅜ
 우리 아이들이 몸도 맘도
 건강하게 밝게 반듯하게
 주님의 거룩한 자녀답게
 매상 성장해주길 정녕 기도했다
 ​
 악한 세상에서 빛과 소금으로
 꿈과 비젼을 품고 살아가길~🙏🏻
 ​
 모든 센스 내고 몸소 만들고~
 오랫만에 작은 재능을 발휘하여
 봉사할 무망지복 있었음에 감사🧡
 할 운명 있을때 최선을 다해 섬겨야지
 주신 자질 썩히지 말고

 여기저기서 사용하자~
 ​
 남은 이틀도 반주로 봉사하며
 은혜의 시간을 보낼듯 하다~^^
 ​
 #여름성경학교 #풍선장식
 #가렌드만들기 #봉사
 #예배 #데코레이션

